import unittest

from article_converters import DocumentConverter, LinkConverter
from tests.stubs import DocumentElementStub


class TestLinkConverter(unittest.TestCase):
    def test_convert_relative_url(self):
        element = self._create_element('/test')
        converter = self._create_converter()
        result = converter.convert(element)
        self.assertEqual('test [http://test.ru/test]', result)

    def test_convert_external_url(self):
        url = 'http://ext.ru'
        element = self._create_element(url)
        converter = self._create_converter()
        result = converter.convert(element)
        self.assertEqual(f'test [{url}]', result)

    @staticmethod
    def _create_element(url):
        return DocumentElementStub('a', 'test', attrs={'href': url})

    @staticmethod
    def _create_converter(base_url='http://test.ru'):
        doc_converter = DocumentConverter()
        converter = LinkConverter(doc_converter)
        converter.current_url = base_url
        return converter


class TestDocumentConverter(unittest.TestCase):
    def test_format_width_simple(self):
        converter = DocumentConverter()
        test_string = f'test {"x" * 75} test'
        result = converter._format_width(test_string)
        result_lines = result.splitlines(False)
        self.assertEqual(2, len(result_lines))
        self.assertEqual(80, len(result_lines[0]))
        self.assertEqual('test', result_lines[1])

    def test_format_width_new_line(self):
        converter = DocumentConverter()
        test_string = f'test {"x" * 75} test \ntest \ntest {"x" * 75} test'
        result = converter._format_width(test_string)
        result_lines = result.splitlines()
        self.assertEqual(5, len(result_lines))
        self.assertEqual('test', result_lines[1])
        self.assertEqual('test', result_lines[2])
