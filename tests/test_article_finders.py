from unittest import TestCase
from unittest.mock import MagicMock

from article_finders import ArticleTagFinder, ArticleClassFinder
from document_reader import DocumentElement
from tests.stubs import DocumentElementStub


class TestArticleTagFinder(TestCase):
    def test_find(self):
        af = ArticleTagFinder()
        root = DocumentElementStub()
        root.children_by_tag = MagicMock(return_value=[DocumentElementStub('article')])
        result = af.find(root)

        root.children_by_tag.assert_called_once_with('article')
        self.assertTrue(result)
        self.assertTrue(af.is_found())

    def test_find_false(self):
        af = ArticleTagFinder()
        root = DocumentElementStub()
        root.children_by_tag = MagicMock(return_value=[])
        result = af.find(root)

        root.children_by_tag.assert_called_once_with('article')
        self.assertFalse(result)
        self.assertFalse(af.is_found())


class TestArticleClassFinder(TestCase):
    def test_find(self):
        af = ArticleClassFinder()
        root = DocumentElementStub()
        root.children_by_attribute_value = MagicMock(return_value=[DocumentElementStub('div')])
        result = af.find(root)

        self.assertTrue(result)
        self.assertTrue(af.is_found())
        root.children_by_attribute_value.assert_called_once_with('class', 'article')
