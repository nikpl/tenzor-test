from document_reader import DocumentElement


class DocumentElementStub(DocumentElement):
    @property
    def parent(self):
        return None

    def attribute(self, name: str):
        return self._attrs.get(name, '')

    @property
    def text(self):
        return self._text

    @property
    def tail(self):
        return ''

    @property
    def tag(self) -> str:
        return self._tag

    @property
    def children(self) -> list:
        return self._children

    def children_by_tag(self, tag: str):
        pass

    def children_by_attribute_value(self, attribute: str, value: str):
        pass

    def __init__(self, tag='', text='', children=None, attrs=None):
        self._tag = tag
        self._text = text
        self._children = children
        self._attrs = attrs or {}
