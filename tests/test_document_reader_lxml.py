from unittest import TestCase

from lxml import html

from document_reader import DocumentElementLxml


def create_html(body_content):
    doc = f"""
           <!DOCTYPE html>
           <head></head>
           <body>
               ${body_content}
           </body>
           """
    return html.document_fromstring(doc)


class TestDocumentReaderLxml(TestCase):
    def test_tag(self):
        dr = DocumentElementLxml(html.document_fromstring('<br/>')[0][0])
        self.assertEqual('br', dr.tag)

    def test_attribute(self):
        dr = DocumentElementLxml(html.document_fromstring('<div class="test"></div>')[0][0])
        self.assertEqual('test', dr.attribute('class'))

    def test_text_and_tail(self):
        dr = DocumentElementLxml(html.document_fromstring('<div>text<span>inspan</span>tail1<span>inpsan</span>tail2'
                                                          '</div>')[0][0])
        self.assertEqual('text', dr.text)
        self.assertEqual('tail1', dr.children[0].tail)
        self.assertEqual('tail2', dr.children[1].tail)
