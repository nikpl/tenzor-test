from abc import ABC, abstractmethod

from lxml import html


class DocumentElement(ABC):
    """Абстрактный класс. Представляет элемент HTML документа."""

    @property
    @abstractmethod
    def tag(self) -> str:
        pass

    @property
    @abstractmethod
    def text(self):
        pass

    @property
    @abstractmethod
    def tail(self):
        pass

    @property
    @abstractmethod
    def children(self) -> list:
        pass

    @property
    @abstractmethod
    def parent(self):
        pass

    @abstractmethod
    def children_by_tag(self, tag: str):
        pass

    @abstractmethod
    def children_by_attribute_value(self, attribute: str, value: str):
        pass

    @abstractmethod
    def attribute(self, name: str):
        pass


class DocumentElementLxml(DocumentElement):
    """Реализация DocumentElement, использующая библиотеку lxml"""

    def attribute(self, name: str):
        return self._element_tree.attrib.get(name, None)

    @property
    def text(self):
        return self._element_tree.text

    @property
    def tail(self):
        return self._element_tree.tail

    def __init__(self, element_tree):
        self._element_tree = element_tree

    @property
    def tag(self) -> str:
        return self._element_tree.tag

    @property
    def children(self) -> list:
        return self._generate_from_elements(self._element_tree)

    @property
    def parent(self):
        return self._element_tree.getparrent()

    def children_by_tag(self, tag: str):
        return self._generate_from_elements(self._element_tree.xpath(f'.//{tag}'))

    def children_by_attribute_value(self, attribute: str, value: str):
        elements = self._element_tree.xpath(f'.//div[contains(@{attribute}, "{value}")]')
        return self._generate_from_elements(elements)

    @staticmethod
    def _generate_from_elements(elements):
        return [DocumentElementLxml(el) for el in elements]


def parse_html(text: str) -> DocumentElement:
    """Преобразует строку с html документом в дерево DocumentElement"""
    return DocumentElementLxml(html.document_fromstring(text))
