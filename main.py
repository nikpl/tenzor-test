import sys

import requests

from article_converters import DocumentConverter, ParagraphConverter, LinkConverter, HeaderConverter
from article_finders import ArticleTagFinder, ArticleClassFinder, LongestTextFinder
from document_reader import parse_html

ARTICLE_FINDERS = [
    ArticleTagFinder,
    ArticleClassFinder,
    LongestTextFinder
]


def main():
    url = get_url_from_arg()
    document = load_document(url)
    converter = create_converter(url)
    length = 0
    article_text = ''
    for ArticleFinder in ARTICLE_FINDERS:
        af = ArticleFinder()
        article = af.find(document)
        if not af.is_found():
            continue
        text = converter.convert(article[0])
        if length < len(text):
            article_text = text
            length = len(text)
    print(article_text)


def create_converter(url):
    doc_conv = DocumentConverter()
    LinkConverter.current_url = url
    doc_conv.add_converters(ParagraphConverter, LinkConverter, HeaderConverter)
    return doc_conv


def get_url_from_arg():
    if len(sys.argv) < 2:
        print('Укажите ссылку на страницу')
        raise Exception()
    return sys.argv[1]


def load_document(url):
    r = requests.get(url)
    doc = parse_html(r.text)
    return doc


main()
