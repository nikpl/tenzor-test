from abc import ABC, abstractmethod
from urllib.parse import urlparse, urljoin

from document_reader import DocumentElement


class ArticleContentConverter(ABC):
    """
    Абстрактный класс, реализации которого, должны отвечать
    за преобразование одного или несклькоьких html-тэгов в текст.
    При преобразовании, может рекурсивно вызвать другие реализации
    ArticleConverter через экземляр DocumentConverter, переданный в
    __init__.
    ВНИМАНИЕ! Реализации не должны хранить внутреннее состояние при
    преобразовании элемента, связанное с этим элементом, это может
    привести к ошибкам, потмоу что преобразование может быть вызвано
    рекурсивно при обработке дерева элементов.

    Аттрибуты:
        _document_converter: экземпляр DocumentConverter, к которому
            принадлежит этот ArticleConverter
    """
    def __init__(self, document_converter):
        self._document_converter = document_converter

    @abstractmethod
    def get_tags(self) -> list:
        """Возвращает список строк с тэгами, за преобразование
        которых отвечает эта реализация ArticleConverter"""
        pass

    @abstractmethod
    def convert(self, element: DocumentElement) -> str:
        """Преобразует элемет html-документа в текст"""
        pass

    def _get_text(self, element: DocumentElement):
        """Возвращает текст для указанного элемента. При этом может быть вызван """
        text = []
        if element.text:
            text.append(element.text.strip())
        if element.children:
            children_text = self._get_text_for_children(element.children)
            text.extend(children_text)
        return ' '.join(text)

    def _get_text_for_children(self, children: list):
        text = []
        for child in children:
            converted = self._document_converter.convert_element(child)
            if converted:
                text.append(converted)
            if child.tail:
                text.append(child.tail.strip())
        return text


class DefaultConverter(ArticleContentConverter):
    """Возвращает текст элемента как есть"""
    def get_tags(self):
        return ['strong', 'b', 'br', 'article']

    def convert(self, element: DocumentElement) -> str:
        return self._get_text(element)


class HeaderConverter(ArticleContentConverter):
    """Преобразует заголовок в строку в верхнем регистре"""
    def get_tags(self):
        return ['h1', 'h2', 'h3', 'h4']

    def convert(self, element: DocumentElement) -> str:
        return f'\n{self._get_text(element).upper()}\n'


class ParagraphConverter(ArticleContentConverter):
    """Преобразует элементы p и div в строку, оканчивающуюся символом новой строки"""
    def get_tags(self):
        return ['p', 'div']

    def convert(self, element_tree) -> str:
        text = self._get_text(element_tree).strip()
        if len(text) > 0:
            text += '\n'
        return text


class LinkConverter(ArticleContentConverter):
    """
    Преобразует тэг 'a' в строку формата 'тексти из строки' [ссылка].
    Для корректного преобразования внутренних и относительных ссылок,
    нужно указать адрес текущей статьи.
    """
    current_url = ''

    def __init__(self, document_converter):
        super().__init__(document_converter)
        self.current_url = LinkConverter.current_url

    def get_tags(self):
        return ['a']

    def convert(self, element: DocumentElement) -> str:
        text = self._get_text(element)
        url = self._get_url(element)
        return f'{text} [{url}]'

    def _get_url(self, element: DocumentElement):
        url = element.attribute("href")
        url = urljoin(self.current_url, url)
        return url


class DocumentConverter:
    """Выполняет преобразование html-документа в текст."""
    def __init__(self):
        self._converters_map = None
        self.line_width = 80
        self.text = []
        self._converters = [DefaultConverter(self)]

    def add_converters(self, *converter_types):
        """
        Добавляет классы реализующие ArticleConverter в список,
        используемых для преобразования докуемента.
        """
        converters = [Con(self) for Con in converter_types]
        self._converters.extend(converters)
        self._converters_map = {}
        for converter in self._converters:
            tags = converter.get_tags()
            for tag in tags:
                self._converters_map[tag] = converter

    def convert(self, root: DocumentElement):
        """
        Преобразует дерево html элементов в текстовый документ.

        Параметры:
            root (DocumentElement): корневой элемент дерева
        """
        text = self.convert_element(root)
        return self._format_width(text)

    def _format_width(self, text: str):
        """
        Форматирует указанный текст так, чтобы длинна одной строки не превышала
        значение указанное в аттрибуте line_width.
        """
        lines = []
        paragraphs = text.splitlines(False)
        for paragraph in paragraphs:
            words = paragraph.strip().split(' ')
            current_line = []
            current_line_length = 0
            for word in words:
                space_count = len(current_line) - 1
                new_line_length = current_line_length + len(word) + space_count
                if new_line_length <= self.line_width:
                    current_line.append(word)
                    current_line_length += len(word)
                else:
                    lines.append(' '.join(current_line))
                    current_line = [word]
                    current_line_length = len(word)
            if len(current_line) > 0:
                lines.append(' '.join(current_line))
        new_text = '\n'.join(lines)
        return new_text

    def convert_element(self, element: DocumentElement):
        converter = self._converters_map.get(element.tag)
        if converter:
            return converter.convert(element)
