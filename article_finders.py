from abc import abstractmethod, ABC
from collections import deque

from document_reader import DocumentElement


class ArticleFinder(ABC):
    """
    Абстрактный класс. Реализации должны просматривать дерево HTML-документа,
    в поисках блока, содержащего статью.
    """

    def __init__(self):
        self._is_found = False

    def find(self, page_tree: DocumentElement) -> list:
        """
        Возвращает список DocumentElement, вероятно, содержащих статью,
        отсортированный в порядке убывания этой вероятности.
        """
        result = self._find(page_tree)
        self._is_found = len(result) > 0
        return result

    @abstractmethod
    def _find(self, page_tree: DocumentElement) -> list:
        # Абстрактный метод. Реализация должна вернуть список DocumentElement,
        # вероятно, содержащих статью, отсортированный в порядке убывания этой
        # вероятности.
        pass

    def is_found(self):
        return self._is_found

    def is_final(self) -> bool:
        """Должен возвращать истину, если найденный элемент точно является статьёй"""
        return False


class ArticleTagFinder(ArticleFinder):
    """Ищет статью по тэгу article"""

    def _find(self, page_tree: DocumentElement):
        return page_tree.children_by_tag('article')

    def is_final(self):
        return True


class ArticleClassFinder(ArticleFinder):
    """Ищет статью по классу, содержащему 'article'"""

    def _find(self, page_tree):
        return page_tree.children_by_attribute_value('class', 'article')


class LongestTextFinder(ArticleFinder):
    def __init__(self):
        super().__init__()
        self.meaning_elements = {'div', 'p', 'strong', 'b', 'br'}
        self._lengths = {}
        self.depth = 2

    def _find(self, page_tree: DocumentElement):
        parents = deque()
        self._count_text_length(page_tree, parents)
        for elements in [k for k in sorted(self._lengths, key=self._lengths.get, reverse=True)]:
            return [elements]
        return []

    def _count_text_length(self, element: DocumentElement, parents: deque):
        # Расчитывает длинну текста для элемента, и рекурсивно для всех вложенных в него элементов.
        # Длинны элементов записываются в аттрибут _lengths. При этом длина текста вложенных элементов
        # добавляется к длинне родительского, но только, если на глубине не больше, указанной в
        # аттрибуте depth.
        is_meaning = element.tag in self.meaning_elements
        length = 0
        if is_meaning and element.text:
            length += len(element.text.strip())
        self._lengths[element] = 0
        for child in element.children:
            if child.tail:
                length += len(child.tail)
            child_parents = parents.copy()
            child_parents.append(element)
            if len(child_parents) > self.depth:
                child_parents.popleft()
            self._count_text_length(child, child_parents)
        for parent in parents:
            self._lengths[parent] += length
        self._lengths[element] += length


